# TP2 : Ethernet, IP, et ARP

## I. Setup IP

🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**

- IP choisies : ```10.33.19.2/26``` et ```10.33.19.10/26```

- Addresse réseau : ```10.33.19.0/26```

- Broadcast : ```10.33.19.63/26```


```
marty@martywork-ROGFlowX13:~$ sudo ifconfig enx144fd7c605d7 10.33.19.2 netmask 255.255.255.192
marty@martywork-ROGFlowX13:~$ ip a
3: enx144fd7c605d7: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:4f:d7:c6:05:d7 brd ff:ff:ff:ff:ff:ff
    inet 10.33.19.2/26 brd 10.33.19.63 scope global enx144fd7c605d7
```

🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**

```
marty@martywork-ROGFlowX13:~$ ping 10.33.19.10
PING 10.33.19.10 (10.33.19.10) 56(84) bytes of data.
64 bytes from 10.33.19.10: icmp_seq=1 ttl=64 time=2.11 ms
```

🌞 **Wireshark it**

**déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par ping**

- Le premier paquet c'est un ICMP request
- le deuxième paquet c'est un ICMP reply

🦈 **PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP**

- Ouvrir le fichier ```ping.pcapng```

## II. ARP my bro
🌞 **Check the ARP table**

```
marty@martywork-ROGFlowX13:~$ arp -a 
? (10.33.19.10) at 00:e0:4c:68:00:b9 [ether] on enx144fd7c605d7
? (10.33.19.10) at <incomplete> on wlp6s0
_gateway (10.33.19.254) at 00:c0:e7:e0:04:4e [ether] on wlp6s0

```


- L'adresse Mac de mon binome ```00:e0:4c:68:00:b```, la Mac de la gateway ```00:c0:e7:e0:04:4e```


🌞 **Manipuler la table ARP**
```
marty@martywork-ROGFlowX13:~$ arp -a
? (10.33.19.10) at 00:e0:4c:68:00:b9 [ether] on enx144fd7c605d7
? (10.33.19.10) at <incomplete> on wlp6s0
_gateway (10.33.19.254) at 00:c0:e7:e0:04:4e [ether] on wlp6s0

marty@martywork-ROGFlowX13:~$ sudo ip -s neigh flush all
marty@martywork-ROGFlowX13:~$ arp -a
_gateway (10.33.19.254) at 00:c0:e7:e0:04:4e [ether] on wlp6s0

marty@martywork-ROGFlowX13:~$ ping 10.33.19.10
PING 10.33.19.10 (10.33.19.10) 56(84) bytes of data.
64 bytes from 10.33.19.10: icmp_seq=2 ttl=64 time=1.45 ms

marty@martywork-ROGFlowX13:~$ arp -a
? (10.33.19.10) at 00:e0:4c:68:00:b9 [ether] on enx144fd7c605d7
_gateway (10.33.19.254) at 00:c0:e7:e0:04:4e [ether] on wlp6s0
```

🌞 **Wireshark it**


**Mettez en évidence les deux trames ARP échangées lorsque vous essayez de contacter quelqu'un pour la "première" fois**

- Dans la première trame mon PC qui demande au broadcast (tout le monde) à qui appartient l'ip inscrite dans le message
- Dans la deuxième trame le PC de mon binome réponds qu'il est le détenteur de la fameuse IP

🦈 **PCAP qui contient les trames ARP**

- fichier ```ARP.capng```



### II.5 Interlude hackerzz

## III. DHCP you too my brooo
🌞 **Wireshark it**

- Trame 1 Discover -> Mon PC demande à tout le monde (broadcast) où est le serveur DHCP pour obtenir une adresse IP
- Trame 2 Offer -> Le serveur DHCP propose à mon PC une adresse IP
- Trame 3 Request -> Je demande à tout le monde si je peux utiliser l'adresse IP proposée
- Trame 4 Ack -> DHCP me confirme l'acquisition de l'adresse IP

🦈 **PCAP qui contient l'échange DORA**

- Ouvrir le fichier ```dhcp.pcapng```



## IV. Avant-goût TCP et UDP
🌞 **Wireshark it**

**déterminez à quelle IP et quel port votre PC se connecte quand vous regardez une vidéo Youtube**

- Le PC se connecte à l'ip ```216.58.214.164``` au port ```443```

🦈 **PCAP qui contient un extrait de l'échange qui vous a permis d'identifier les infos**

- Ouvrir le fichier ```ytb.pcapng```


**_Un seul rendu pour deux avec DUNIAUD Marty_**
