import socket
import sys


def Main():
    if len(sys.argv) < 3:
        print("Entrez une IP et un Port ")
        sys.exit()

    ip = str(sys.argv[1])
    port = int(sys.argv[2])
    print("Le client est sur l'Host " + ip + "| Port " + str(port))

    s = socket.socket()
    s.connect((ip, port))

    print("Deux joueurs? (y/n)")
    print(">>", end='')
    msg = input().lower()

    while 1:
        if msg == 'y' or msg == 'n':
            break
        msg = input('Veuillez choisir y (Yes) or n (No)')

    if msg == 'y':
        # Signal game start (2P)
        twoPlayerSignal = '2'.encode('utf-8')
        s.send(twoPlayerSignal)
        playGame(s)

    else:
        # Signal game (1P)
        twoPlayerSignal = '0'.encode('utf-8')
        s.send(twoPlayerSignal)

        print("1 joueur, la partie commence")
        playGame(s)


def recv_helper(socket):
    first_byte_value = int(socket.recv(1)[0])
    if first_byte_value == 0:
        x, y = socket.recv(2)
        return 0, socket.recv(int(x)), socket.recv(int(y))
    else:
        return 1, socket.recv(first_byte_value)


def playGame(s):
    while True:
        pkt = recv_helper(s)
        msgFlag = pkt[0]
        if msgFlag != 0:
            msg = pkt[1].decode('utf8')
            print(msg)
            if msg == 'server-overloaded' or 'Game Over!' in msg:
                break
        else:
            gameString = pkt[1].decode('utf8')
            incorrectGuesses = pkt[2].decode('utf8')
            print(" ".join(list(gameString)))
            print("Mauvaix choix: " + " ".join(incorrectGuesses) + "\n")
            if "_" not in gameString or len(incorrectGuesses) >= 6:
                continue
            else:
                letterGuessed = ''
                valid = False
                while not valid:
                    letterGuessed = input('Lettre à deviner: ').lower()
                    if letterGuessed in incorrectGuesses or letterGuessed in gameString:
                        print("Erreur! Lettre " + letterGuessed.upper() +
                              " a déjà été trouvé, trouvez une autre lettre.")
                    elif len(letterGuessed) > 1 or not letterGuessed.isalpha():
                        print("Erreur! Veuillez choisir UNE seule lettre")
                    else:
                        valid = True
                msg = bytes([len(letterGuessed)]) + \
                    bytes(letterGuessed, 'utf8')
                s.send(msg)

    s.shutdown(socket.SHUT_RDWR)
    s.close()


if __name__ == '__main__':
    Main()
