# Game and protocol

Petit programme Hangman(jeu du pendu pour les 100% gaulois) avec un serveur local pour pouvoir jouer à deux (ou seule si pas d'amis😢) .

## Lancement du serveur

```
python3 server.py [Numéro de Port]
```

## Lancement du client

```
python3 client.py [IP du serveur][Numéro de Port du serveur]
```

<br/>

**Il ne vous reste plus qu'à vous amuser 😉 !**

