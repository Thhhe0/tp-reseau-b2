# TP1 - Mise en jambes

TP réalisé sous MAC OS
## 1. Affichage d'informations sur la pile TCP/IP locale
**🌞 Affichez les infos des cartes réseau de votre PC**

**En ligne de commande**

Commande :
```ifconfig```

résultat  interface wifi :
```

en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu
	options=400<CHANNEL_IO>
	ether 3c:a6:f6:2c:bf:5b 
	inet6 fe80::1093:3b20:d810:42a0%en0 prefixlen 64 secured scopeid 0xb 
	inet 10.33.18.99 netmask 0xfffffc00 broadcast 10.33.19.255
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active

```



**Nom = en0 
Adresse mac = ether 3c:a6:f6:2c:bf:5b 
Adresse ip = inet 10.33.18.99**

<br/>

Étant sur MacBook je n'ai pas d'interface Ethernet.
<br/>
**🌞 Affichez votre gateway**
Commande :
````route get default | grep gateway````

Résultat :
```gateway : 10.33.19.254```
<br/>

**En graphique (GUI : Graphical User Interface)**
**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**
* Cliquez sur le **Logo Apple (tout en haut à gauche de votre ordi)** > **Préférences Système** 
* Allez sur le menu **Réseau** 
* Puis cliquez sur **avancé... (en bas à droite)**
* Dans l'onglet **TCP/IP** vous retrouverez votre adresse IP, ainsi que votre gateway (nommé routeur)
* Et dans l'onglet matériel vous y retrouverez votre adresse MAC 
<br/>

**Questions**
🌞 à quoi sert la gateway dans le réseau d'YNOV ?

- Elle sert à pouvoir communiqué avec un autre réseau.

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)
🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

On refait le même chemin que précedemment :
1. **Préférence système**
2. **Réseau**
3. **Avancé**
4. **TCP/IP** 
5. **Puis on choisit "manuellement" dans la liste déroulante**

<br/>

🌞 **Il est possible que vous perdiez l'accès internet. Expliquer pourquoi**
Si l'on perd l'accès c'est tout simplement car l'IP qu'on a mit est déjà utilisé par quelqu'un d'autre.


## II. Exploration locale en duo

  (Réalisé sur le pc de deux membre de mon groupe car je n'ai pas de RJ45)

### 1. Modification d'adresse IP

  

Via la même manipulation mais cette fois sur Ethernet j'ai pu changer l'adresse IP :

  

```

PS C:\Users\gympl> ipconfig

[...]

Carte Ethernet Ethernet :

[...]

Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.102

Masque de sous-réseau. . . . . . . . . : 255.255.255.252

```

Test ping d'une machine à l'autre :

```

PS C:\Users\gympl> ping 192.168.1.101

  

Envoi d’une requête 'Ping' 192.168.1.101 avec 32 octets de données :

Réponse de 192.168.1.101 : octets=32 temps=1 ms TTL=128

```

Affichage de la table ARP :

```

PS C:\Users\gympl> arp -a

[...]

Interface : 192.168.1.102 --- 0x6

Adresse Internet Adresse physique Type

192.168.1.101 98-28-a6-16-1f-c4 dynamique

192.168.1.103 ff-ff-ff-ff-ff-ff statique

224.0.0.22 01-00-5e-00-00-16 statique

224.0.0.251 01-00-5e-00-00-fb statique

224.0.0.252 01-00-5e-00-00-fc statique

239.255.255.250 01-00-5e-7f-ff-fa statique

255.255.255.255 ff-ff-ff-ff-ff-ff statique

[...]

```

  

### 2. Utilisation d'un des deux comme gateway

  

Sur le PC qui où Internet est désactivé :

```

Modification de la passerelle par l'ip du pc ayant la connection à savoir :

192.168.1.102

```

Sur le PC qui où Internet est activé :

```

Dans le panneau de configuration -> Réseau et Internet -> Etat de Wifi -> Propriétés de Wifi-> Partage -> activer le partage de wifi via Ethernet.

```

Résolution de ping vers serveur google :

```

PS C:\Users\Lucas> ping 8.8.8.8 Envoi d’une requête 'Ping' 8.8.8.8 avec 32 octets de données :

Réponse de 8.8.8.8 : octets=32 temps=25 ms TTL=113

```

Utilisation traceroute

```

PS C:\Users\Lucas> tracert google.com

Détermination de l’itinéraire vers google.com [216.58.215.46] avec un maximum de 30 sauts :

1 <1 ms * <1 ms LAPTOP-2P72Q5OG [192.168.137.1]

```

### 3. Petit chat privé

PC Serveur :

```

PS C:\Users\Lucas\Desktop\netcat-1.11> .\nc.exe -l -p 8888

```

PC Client :

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> .\nc.exe 192.168.137.2 8888

```

  

Discussion intéréssante :

```

PS C:\Users\gympl\OneDrive\Bureau\netcat-1.11> .\nc.exe 192.168.137.2 8888

fpobs

gros fdp

téo c un boffon

```

Allons plus loin dans le chat :

```

PS C:\Users\Lucas\Desktop\netcat-1.11> .\nc.exe -l -p 8888 192.168.137.1

```

Seul l'IP marquée dans la commande peut se connecter

  

### 4. Firewall

  

🌞 Autoriser les ping


Autorisation des pings :

Dans un powershell en mode admin :

```

netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow

```

PC 1 :

![Screen PC 1](https://i.ibb.co/tp26hyr/image-2022-09-28-161716071.png)

PC 2

![Screen PC 2](https://cdn.discordapp.com/attachments/887698327066529842/1024685734868815892/2022-09-28_16_13_57-Pare-feu_Windows_Defender.png)


🌞 Autoriser le traffic sur le port qu'utilise nc


Autorisation Netcat sur le port 8888:

```

PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name= "Open Port 8888" dir=in action=allow protocol=TCP localport=8888

Ok.

```

## III. Manipulations d'autres outils/protocoles côté client

  

### 1. DHCP

🌞Exploration du DHCP, depuis votre PC

```

cd /var/db/dhcpclient/leases/

cat en0.plist 

[...]

<key>IPAddress</key>
	<string>10.33.18.99</string>	
```

Date d'expiration du bail DHCP (exprimé en seconde):

```
<integer>79798</integer>
```

  
 <br/>

### 2. DNS

  
🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

**Préférence système > Réseau > Avancé... > DNS**

<br/>

🌞 utiliser, en ligne de commande l'outil nslookup (Windows, MacOS) ou dig (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

#### nslookup :

- Google :

```

theo_l@Air-de-Theo ~ % nslookup google.com
Server:		2001:861:c4:d050:3293:bcff:fedc:c325
Address:	2001:861:c4:d050:3293:bcff:fedc:c325#53

Non-authoritative answer:
Name:	google.com
Address: 216.58.198.206

```

A l'adresse IP 216.58.198.206 se trouve le nom de domaine google.com.

  
<br/>

- Ynov :

```

theo_l@Air-de-Theo ~ % nslookup ynov.com  
Server:		2001:861:c4:d050:3293:bcff:fedc:c325
Address:	2001:861:c4:d050:3293:bcff:fedc:c325#53

Non-authoritative answer:
Name:	ynov.com
Address: 172.67.74.226
Name:	ynov.com
Address: 104.26.10.233
Name:	ynov.com
Address: 104.26.11.233

```

Aux adresses IP 104.26.10.233, 172.67.74.226, 104.26.11.233 se trouve le nom de domaine ynov.com.


<br/>

#### Reverse lookup

  

- 78.74.21.21

```

theo_l@Air-de-Theo ~ % nslookup 78.74.21.21       
Server:		2001:861:c4:d050:3293:bcff:fedc:c325
Address:	2001:861:c4:d050:3293:bcff:fedc:c325#53

Non-authoritative answer:
21.21.74.78.in-addr.arpa	name = host-78-74-21-21.homerun.telia.com.

Authoritative answers can be found from:

```

A l'adresse IP 78.74.21.21 se trouve le nom de domaine homerun.telia.com

  

- 92.146.54.88

```

theo_l@Air-de-Theo ~ % nslookup 92.146.54.88
Server:		2001:861:c4:d050:3293:bcff:fedc:c325
Address:	2001:861:c4:d050:3293:bcff:fedc:c325#53

** server can't find 88.54.146.92.in-addr.arpa: NXDOMAIN

```

L'adresse IP 92.146.54.88 ne correspond à aucun nom de domaine.

  

## IV. Wireshark

  -   Téléchargez l'outil [Wireshark](https://www.wireshark.org/)
-   🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :

- Ping entre moi et la passerelle :

```

233 64.960857 192.168.137.2 192.168.137.1 ICMP 74 Echo (ping) request id=0x0001, seq=227/58112, ttl=128 (reply in 234)

```

  

- Message Netcat entre moi et l'autre PC :

```

42 12.742014 192.168.137.2 192.168.137.1 TCP 64 8888 → 33377 [PSH, ACK] Seq=1 Ack=5 Win=2097920 Len=10

```

  

- Requête DNS :

```

70 50.668970314 192.168.137.2 8.8.4.4 DNS 100 Standard query 0x288f A clientservices.googleapis.com OPT

```

c'est une requête aux serveurs DNS de google.
