# TP4 : TCP, UDP et services réseau
## I. First steps

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

- **Deezer** : 
TCP
IP + Port  = 78.40.123.97:443
Port Local = 63782
- **Youtube** : 
UDP
IP + Port  = 142.250.75.238:443
Port Local = 65055
- **Twitter** : 
TCP
IP + Port  = 104.244.42.65:443
Port Local = 52884
- **Discord** :
TCP
IP + Port  =  162.159.130.234:443 
Port Local = 52903
- **Twitch** : 
TCP
IP + Port  = 151.101.38.167 : 443
Port Local = 52933

🌞 **Demandez l'avis à votre OS**

**Deezer**
```
theo_l@MacBook-Air-de-Theo ~ % netstat -n -p tcp -b | grep 78.40.123.98
tcp4       0      0  192.168.1.58.53030     78.40.123.98.443       ESTABLISHED      10899       5731
```

**Youtube**
```
Pas trouvé
```

**Twitter**
```
theo_l@MacBook-Air-de-Theo ~ % netstat -n -p tcp -b | grep 104.244.42
tcp4       0      0  192.168.1.58.53157     104.244.42.129.443     ESTABLISHED       1695       2951
```
**Discord**
```
theo_l@MacBook-Air-de-Theo ~ % netstat -n -p tcp -b | grep 162.159.13 
tcp4       0      0  192.168.1.58.53003     162.159.135.234.443    ESTABLISHED     806522      53874
```

**Twitch**
```
theo_l@MacBook-Air-de-Theo ~ % netstat -n  -p tcp -b | grep 151.101.38.167
tcp4       0      0  192.168.1.58.53161     151.101.38.167.443     ESTABLISHED      17493       6225
```
<br/>

🦈🦈🦈🦈🦈 Checker les captures wireshark dans le dossier  *_Capture Wireshark_*

## II. Mise en place

### 1. SSH
```
theo_l@MacBook-Air-de-Theo ~ % ssh theol@10.211.55.28
theol@10.211.55.28's password: 
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Oct 12 17:37:56 2022 from 10.211.55.2
[theol@rocky ~]$
```

🌞 **Examinez le trafic dans Wireshark**

- **déterminez si SSH utilise TCP ou UDP**
En SSH on utilise du TCP car ils faut que la communication soit fiable et contrôlé.

- **repérez le _3-Way Handshake_ à l'établissement de la connexion**
Indispo

- **repérez le FIN FINACK à la fin d'une connexion**
Indispo

🌞 **Demandez aux OS**

- **repérez, avec un commande adaptée, la connexion SSH depuis votre machine**
```
theo_l@Air-de-Theo ~ % netstat -n -p tcp | grep 10.211.55.28
tcp4       0      0  10.211.55.2.53382      10.211.55.28.22        ESTABLISHED
```
- **ET repérez la connexion SSH depuis votre VM**
```
[theol@rocky ~]$ ss -tpn 
State   Recv-Q   Send-Q     Local Address:Port     Peer Address:Port   Process  
ESTAB   0        0           10.211.55.28:22        10.211.55.2:53382 
```
### 2. NFS

🌞 **Mettez en place un petit serveur NFS sur l'une des deux VMs**

NFS Serveur config :

```
[theol@localhost ~]$ dnf -y install nfs-utils
[theol@localhost ~]$ sudo nano /etc/idmapd.conf
[theol@localhost ~]$ cat /etc/idmapd.conf | head -n5 | tail -n1
Domain = srv.world
[theol@localhost ~]$ sudo nano /etc/exports
[theol@localhost ~]$ cat /etc/exports
/srv/nfsshare 192.168.1.58/24(rw,no_root_squash)
[theol@localhost ~]$ mkdir /srv/nfsshare
[theol@localhost ~]$ sudo systemctl enable --now rpcbind nfs-server
[theol@localhost ~]$ sudo firewall-cmd --add-service=nfs
 success
[marty@localhost ~]$ sudo firewall-cmd --runtime-to-permanent
 success
```

NFS Client config :

**Failed**

🌞 **Wireshark it !**


**Failed**

🌞 **Demandez aux OS**
**Failed**


### 3. DNS

🌞 Utilisez une commande pour effectuer une requête DNS depuis une des VMs

**Failed**
